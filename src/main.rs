// mod htb_api;

use serenity::async_trait;
use serenity::client::{Client, Context, EventHandler};
use serenity::model::channel::{Message};
use serenity::http::Http;
use serenity::framework::standard::{
    StandardFramework,
    CommandResult,
    macros::{
        command,
        group
    }
};

use std::collections::HashMap;
use std::{env, thread};
use json::JsonValue;
use serenity::builder::CreateMessage;
use dotenv;
use serenity::model::id::ChannelId;

mod dbconn;
mod hacker;
mod autoupdate;

use self::hacker::Hacker;

#[group]
#[commands(user, leaderboard, add_user, init)]
struct General;

struct Handler;

#[async_trait]
impl EventHandler for Handler {}

#[tokio::main]
async fn main() {
    let framework = StandardFramework::new()
        .configure(|c| c.prefix("~"))
        .group(&GENERAL_GROUP);

    dotenv::dotenv().ok();

    let token = env::var("DISCORD_TOKEN").expect("token");
    let mut client = Client::builder(token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Error creating client");
    // start listening for events by starting a single shard
    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {:?}", why);
    }
}

#[command]
async fn user(ctx: &Context, msg: &Message) -> CommandResult {
    let cmd: Vec<&str>= (&msg.content).split_whitespace().collect();
    let user_id: &str = cmd[1];

    let req_url = format!("https://www.hackthebox.eu/api/v4/profile/{}", user_id);
    let response = reqwest::get(&req_url).await?;
    let account: JsonValue = json::parse(&response.text().await?).unwrap();

    if account.has_key("profile") {
        let json: HashMap<&str, &JsonValue> = account.entries().collect();
        let profile:  HashMap<&str, &JsonValue> = json["profile"].entries().collect();

        let name = profile["name"];
        let rank = profile["rank"];
        let next_rank = profile["next_rank"];
        let ranking = profile["ranking"];
        let rooted = profile["system_owns"];
        let user_own = profile["user_owns"];

        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title("HackTheBot");
                e.description(format!("Overview of {}'s profile !", &name));
                e.field("Rank", &rank, true);
                e.field("Next Rank", &next_rank, true);
                e.field("Ranking", &ranking, true);
                e.field("Owned Machines", format!("{}", &rooted), true);
                e.field("User Machines", format!("{}", &user_own), true);
                return e;
            });
            return m;
        }).await?;
    } else {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title("HackTheBot");
                e.description(format!("Couldn't find user profile for id: {}", user_id));
                return e;
            });
            return m;
        }).await?;
    }
    return Ok(());
}

#[command]
async fn leaderboard(ctx: &Context, msg: &Message) -> CommandResult {
    let cmd : Vec<&str>= (&msg.content).split_whitespace().collect();
    let mut option: &str = "";
    if cmd.len() >= 2 {
        option = cmd[1];
    }

    if option == "global" {
        // retrieve users
        let mut users: Vec<Hacker> =  dbconn::get_users().await;
        if users.len() > 1 {
            users.sort_by(|a, b| b.points.cmp(&a.points));
        }

        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title("Leaderboard");
                e.description("HackTheBox Scoreboard");
                for hacker in users.iter() {
                    let user = &hacker;
                    e.field(format!("{} ({})", user.name, user.id),
                            format!("{} Points, {} roots, {} users", user.points, user.root_owns, user.user_owns),
                            false);
                }
                return e;
            });
            return m;
        }).await?;

    } else if option == "active" {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title("Scoreboard");
                e.description("Cette option arrivera prochainement");
                return e;
            });
            return m;
        }).await?;
    } else {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title("Scoreboard");
                e.description("Syntax: ~scoreboard [global|active]");
                return e;
            });
            return m;
        }).await?;
    }

    return Ok(());
}

#[command]
async fn add_user(ctx: &Context, msg: &Message) -> CommandResult {
    let cmd: Vec<&str> = (&msg.content).split_whitespace().collect();
    let user_id: &str = cmd[1];

    let req_url = format!("https://www.hackthebox.eu/api/v4/profile/{}", user_id);
    let response = reqwest::get(&req_url).await?;
    let account: JsonValue = json::parse(&response.text().await?).unwrap();

    if account.has_key("profile") {
        dbconn::add_user(account).await.expect("Couldn't insert user.");
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title("HackTheBot");
                e.description("User successfully added !");
                return e;
            });
            return m;
        }).await?;
    } else {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title("HackTheBox");
                e.description(format!("Couldn't find user profile for id: {}", user_id));
                e.field("Hint:", "Maybe set your profile to public ?", true);
                return e;
            });
            return m;
        }).await?;
    }

    return Ok(());
}

#[command]
async fn init(_ctx: &Context, msg: &Message) -> CommandResult {
    let id = u64::from(msg.channel_id);
    autoupdate::autoupdate(id).await?;
    Ok(())
}