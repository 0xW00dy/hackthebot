use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize)]
pub struct Hacker {
    pub id: i32,
    pub name: String,
    pub root_owns: i32,
    pub user_owns: i32,
    pub points: i32
}