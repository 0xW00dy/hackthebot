use std::collections::HashMap;
use json::JsonValue;
use mongodb::{bson::doc, options::ClientOptions, Client, Collection};
use mongodb::error::Error;
use mongodb::options::FindOptions;
use serenity::futures::{TryStreamExt};

use std::ops::Not;

use super::hacker::Hacker;


pub async fn get_client() -> Client {
    let mut client_options = ClientOptions::parse("mongodb://root:example@localhost:27017/").await.expect("Error connecting");

    client_options.app_name = Some("HacKTheBot".to_string());

    let client = mongodb::Client::with_options(client_options).expect("Couldn't load options");

    client
        .database("admin")
        .run_command(doc! {"ping":1}, None)
        .await
        .expect("Couldn't connect");

    return client;
}

pub async fn is_user_in_database(client: Client, id: i32) -> bool {
    let users_collection: Collection<Hacker> = client.database("HackTheBot").collection("users");
    println!("{}", id);
    let cursor = users_collection.distinct("id", None, None).await.expect("asdasdas");
    for i in cursor.iter() {
        if i.as_i32() == Some(id) {
            return true;
        }
    }
    return false;
}

pub async fn add_user(user: JsonValue) -> Result<(), Error> {
    let client = get_client().await;
    let users_collection = client.database("HackTheBot").collection("users");

    let json: HashMap<&str, &JsonValue> = user.entries().collect();
    let user_json: HashMap<&str, &JsonValue> = json["profile"].entries().collect();

    let user_id = user_json["id"].as_i32();

    let user_data = doc! {
        "id": user_id,
        "name": user_json["name"].as_str(),
        "rank": user_json["rank"].as_str(),
        "root_owns": user_json["system_owns"].as_i32(),
        "user_owns": user_json["user_owns"].as_i32(),
        "points": user_json["points"].as_i32()
    };

    if is_user_in_database(client, user_id.unwrap()).await.not() {
        users_collection.insert_one(user_data, None).await;
    } else {
        let filter = doc! {"id": user_id.unwrap()};
        let update = doc!{"$set": user_data};
        users_collection.update_one(filter, update, None).await;
    }
    Ok(())
}

pub async fn get_users() -> Vec<Hacker> {
    let client= get_client().await;
    let users_collection: Collection<Hacker> = client.database("HackTheBot").collection("users");
    let find_options = FindOptions::builder().build();
    let filter = doc! {};
    let mut user_cursor = users_collection.find(filter, find_options).await.expect("Couldn't find anything : (");

    let mut user_list: Vec<Hacker> = Vec::new();
    while let Some(hacker) = user_cursor.try_next().await.expect("Truc") {
        user_list.push(hacker);
    }

    return user_list;
}

pub async fn update_user() -> Result<(), Error> {

    Ok(())
}