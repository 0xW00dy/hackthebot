use std::collections::HashMap;
use std::{env, thread};
use std::time::Duration;
use json::JsonValue;
use serenity::http::Http;
use serenity::model::error::Error;
use serenity::model::id::ChannelId;


use crate::hacker::Hacker;
use crate::dbconn::{get_users, add_user};


pub async fn autoupdate(id: u64) -> Result<(), Error> {
    let token = env::var("DISCORD_TOKEN").expect("token");
    let channel_id = ChannelId::from(id);
    loop {
        let hackers_list: Vec<Hacker> = get_users().await;
        for hacker in hackers_list {
            let user = &hacker;

            let req_url = format!("https://www.hackthebox.eu/api/v4/profile/{}", user.id.to_string());
            let response = reqwest::get(&req_url).await.expect("Request failed.");
            let response_text = &response.text().await.expect("Couldn't get text of response");

            let char_vec: Vec<char> = response_text.chars().collect();
            if char_vec[0] == '{' {
                let account: JsonValue = json::parse(response_text).unwrap();
                if account.has_key("profile") {
                    let json: HashMap<&str, &JsonValue> = account.entries().collect();
                    let profile: HashMap<&str, &JsonValue> = json["profile"].entries().collect();
                    let user_owns = profile["user_owns"].as_i32().unwrap();
                    let system_owns = profile["system_owns"].as_i32().unwrap();

                    if user_owns != user.user_owns {
                        let ctx = Http::new_with_token(&token);
                        let user_profile = profile.clone();
                        let user_account = account.clone();
                        user_own(user_profile, ctx, channel_id).await;
                        add_user(user_account).await.expect("Couldn't insert user");
                    }
                    if system_owns != user.root_owns {
                        let ctx = Http::new_with_token(&token);
                        let user_profile = profile.clone();
                        let user_account = account.clone();
                        system_own(user_profile, ctx, channel_id).await;
                        add_user(user_account).await.expect("Couldn't insert user");
                    }
                }
                thread::sleep(Duration::from_secs(10));
            } else {
                println!("Error handling server response. Skipping");
            }
        }
    }
}

pub async fn get_activity(id: i32) -> HashMap<&'static str, String> {
    let mut ret: HashMap<&str, String> = HashMap::new();
    let req_url = format!("https://www.hackthebox.eu/api/v4/profile/activity/{}", id);
    let response = reqwest::get(&req_url).await.expect("Couldn't fetch activity");
    let response_text = &response.text().await.expect("Couldn't get text of response");

    let char_vec: Vec<char> = response_text.chars().collect();
    if char_vec[0] == '{' {
        let activity: JsonValue = json::parse(response_text).unwrap();
        if activity.has_key("profile") {
            let json: HashMap<&str, &JsonValue> = activity.entries().collect();
            let profile: HashMap<&str, &JsonValue> = json["profile"].entries().collect();
            let activities: &JsonValue = profile["activity"];

            let machine_name = activities[0]["name"].as_str().unwrap().to_string();

            ret.insert(
                "machine_name",
                machine_name,
            );
        }
    } else {
        println!("Error handling server response. Skipping");
    }
    return ret;
}

pub async fn user_own(profile: HashMap<&str, &JsonValue>, ctx: Http, channel_id: ChannelId) {
    let infos = get_activity(profile["id"].as_i32().unwrap()).await;
    let machine_name = &infos["machine_name"];
    channel_id.send_message(ctx, |m| {
        m.embed(|e| {
            e.title(format!("{} owned user om {} !", profile["name"], machine_name));
            return e;
        });
        return m;
    }).await.expect("Couldn't send message");
}

pub async fn system_own(profile: HashMap<&str, &JsonValue>, ctx: Http, channel_id: ChannelId) {
    let infos = get_activity(profile["id"].as_i32().unwrap()).await;
    let machine_name = &infos["machine_name"];
    channel_id.send_message(ctx, |m| {
        m.embed(|e| {
            e.title(format!("{} owned system on {} !", profile["name"], machine_name));
            return e;
        });
        return m;
    }).await.expect("Couldn't send message");
}